﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HidLibrary;
using System.Timers;

namespace BuzzWireless
{
    enum BuzzButtonCode
    {
        Controller1R = 0x000001,
        Controller1Y = 0x000002,
        Controller1G = 0x000004,
        Controller1O = 0x000008,
        Controller1B = 0x000010,

        Controller2R = 0x000020,
        Controller2Y = 0x000040,
        Controller2G = 0x000080,
        Controller2O = 0x000100,
        Controller2B = 0x000200,

        Controller3R = 0x000400,
        Controller3Y = 0x000800,
        Controller3G = 0x001000,
        Controller3O = 0x002000,
        Controller3B = 0x004000,

        Controller4R = 0x008000,
        Controller4Y = 0x010000,
        Controller4G = 0x020000,
        Controller4O = 0x040000,
        Controller4B = 0x080000,
    };



    class BuzzDevice
    {
        private const int _inputFrequency = 100;
        private const int _vid = 0x054C;
        private const int _pid = 0x1000;

        private bool _isDongleAttached;
        private int _buttonStates;

        private Dictionary<int, BuzzButton> _buttons;
        private Timer _timerDongleListener;
        private Timer _timerInputListener;
        private HidDevice _dongle;
        private HidReport _report;

        public event EventHandler<BuzzButtonEventArgs> ButtonChanged;
        public event EventHandler<BuzzButtonEventArgs> ButtonPressed;
        public event EventHandler<BuzzButtonEventArgs> ButtonReleased;

        public bool IsAttached { get { return _isDongleAttached; } }



        protected virtual void OnButtonPressed(BuzzButtonEventArgs e)
        {
            ButtonPressed?.Invoke(this, e);
        }

        protected virtual void OnButtonReleased(BuzzButtonEventArgs e)
        {
            ButtonReleased?.Invoke(this, e);
        }

        protected virtual void OnButtonChanged(BuzzButtonEventArgs e)
        {
            Console.WriteLine(e.Button.Player + " " + e.Button.Value + ": " + e.State.ToString());

            ButtonChanged?.Invoke(this, e);

            if(e.State)
            {
                OnButtonPressed(e);
            } else
            {
                OnButtonReleased(e);
            }
        }


        public BuzzDevice()
        {
            _isDongleAttached = false;
            _report = null;

            _timerDongleListener = new Timer();
            _timerDongleListener.Interval = 200;
            _timerDongleListener.Elapsed += FindBuzzDongle;
            _timerDongleListener.Enabled = true;

            _timerInputListener = new Timer();
            _timerInputListener.Interval = 1000 / _inputFrequency;
            _timerInputListener.Elapsed += ReadInput;
            _timerInputListener.Enabled = true;

            MapButtons();
        }

        /// <summary>
        /// Creates a dictionary where we store all buttoncombinations.
        /// </summary>
        private void MapButtons()
        {
            _buttons = new Dictionary<int, BuzzButton>();

            _buttons.Add((int)BuzzButtonCode.Controller1R, new BuzzButton(1, "R"));
            _buttons.Add((int)BuzzButtonCode.Controller1Y, new BuzzButton(1, "Y"));
            _buttons.Add((int)BuzzButtonCode.Controller1G, new BuzzButton(1, "G"));
            _buttons.Add((int)BuzzButtonCode.Controller1O, new BuzzButton(1, "O"));
            _buttons.Add((int)BuzzButtonCode.Controller1B, new BuzzButton(1, "B"));

            _buttons.Add((int)BuzzButtonCode.Controller2R, new BuzzButton(2, "R"));
            _buttons.Add((int)BuzzButtonCode.Controller2Y, new BuzzButton(2, "Y"));
            _buttons.Add((int)BuzzButtonCode.Controller2G, new BuzzButton(2, "G"));
            _buttons.Add((int)BuzzButtonCode.Controller2O, new BuzzButton(2, "O"));
            _buttons.Add((int)BuzzButtonCode.Controller2B, new BuzzButton(2, "B"));

            _buttons.Add((int)BuzzButtonCode.Controller3R, new BuzzButton(3, "R"));
            _buttons.Add((int)BuzzButtonCode.Controller3Y, new BuzzButton(3, "Y"));
            _buttons.Add((int)BuzzButtonCode.Controller3G, new BuzzButton(3, "G"));
            _buttons.Add((int)BuzzButtonCode.Controller3O, new BuzzButton(3, "O"));
            _buttons.Add((int)BuzzButtonCode.Controller3B, new BuzzButton(3, "B"));

            _buttons.Add((int)BuzzButtonCode.Controller4R, new BuzzButton(4, "R"));
            _buttons.Add((int)BuzzButtonCode.Controller4Y, new BuzzButton(4, "Y"));
            _buttons.Add((int)BuzzButtonCode.Controller4G, new BuzzButton(4, "G"));
            _buttons.Add((int)BuzzButtonCode.Controller4O, new BuzzButton(4, "O"));
            _buttons.Add((int)BuzzButtonCode.Controller4B, new BuzzButton(4, "B"));
        }

        private void ReadInput(object sender = null, ElapsedEventArgs e = null)
        {
            if (!_isDongleAttached) { return; }

            var data = _dongle.ReadReport();
            var newButtonStates = (data.Data[4] & 15) << 16 | data.Data[3] << 8 | data.Data[2];

            if (newButtonStates != _buttonStates)
            {
                // Check which buttons have changed
                var stateDiff = _buttonStates ^ newButtonStates;
                var buttonMasks = FindSetBitMasks(stateDiff);

                foreach(var mask in buttonMasks)
                {
                    var button = _buttons[mask];
                    var state = (newButtonStates & mask) > 0;

                    OnButtonChanged(new BuzzButtonEventArgs(button, state));
                }
                
                _buttonStates = newButtonStates;
            }
        }

        /// <summary>
        /// Grabs the last three bytes of the inputdata, and puts them in the most sensible order.
        /// </summary>
        /// <returns></returns>
        public string DataToString()
        {
            string ret = "";

            ret += string.Format("{0}", Convert.ToString(_report.Data[4], 2)).PadLeft(8, '0') + " ";
            ret += string.Format("{0}", Convert.ToString(_report.Data[3], 2)).PadLeft(8, '0') + " ";
            ret += string.Format("{0}", Convert.ToString(_report.Data[2], 2)).PadLeft(8, '0') + " ";

            return ret;
        }


        private void FindBuzzDongle(object sender = null, ElapsedEventArgs e = null)
        {
            _dongle = (from a in HidDevices.Enumerate()
                       where a.Attributes.VendorId == _vid && a.Attributes.ProductId == _pid
                       select a).SingleOrDefault();

            // Connecteer als we hem gevonden hebben
            if (_dongle != null)
            {
                Console.WriteLine("Dongle found");

                _dongle.OpenDevice();
                _dongle.MonitorDeviceEvents = true;
                _dongle.Removed += Buzz_Removed;
                _dongle.Inserted += Buzz_Inserted;

                _timerDongleListener.Enabled = false;
                _isDongleAttached = true;

                AwakenHandsets();
            }
        }

        public void AwakenHandsets()
        {
            WriteReport(new byte[] { 0, 0, 0, 0, 0, 0, 0 });
        }

        public void ControlLights()
        {
            WriteReport(new byte[] { 00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF });
        }


        private void WriteReport(byte[] data, int mode = 2, HidDeviceData.ReadStatus status = HidDeviceData.ReadStatus.Success)
        {
            if (!_isDongleAttached) { return; }

            var report = new HidReport(data.Length, new HidDeviceData(data, status));

            // Laatste lijkt sneller te zijn...
            switch (mode)
            {
                case 1: _dongle.WriteReport(report); break;
                case 2: _dongle.Write(data); break;
            }
        }


        private void Buzz_Inserted()
        {
            _isDongleAttached = true;

            Console.WriteLine("Dongle attached");
        }

        private void Buzz_Removed()
        {
            _isDongleAttached = false;

            Console.WriteLine("Dongle detached");
        }


        private bool IsBitSet(int Number, int pos)
        {
            return (Number & (1 << pos)) != 0;
        }

        private List<int> FindSetBitMasks(int Number)
        {
            List<int> Masks = new List<int>();

            for(int i=0; i<32; i++)
            {
                if(IsBitSet(Number, i)) { Masks.Add((int)Math.Pow(2, i)); }
            }

            return Masks;
        }
    }

   

    public class BuzzButton
    {
        public int Player { get; set; }
        public string Value { get; set; }

        public BuzzButton(int Player, string Value)
        {
            this.Player = Player;
            this.Value = Value;
        }
    }

}

