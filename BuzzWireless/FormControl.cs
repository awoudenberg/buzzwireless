﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzzWireless
{
    public partial class FormControl : Form
    {
        public FormDisplay Display;

        delegate void FunctionCallback();
        delegate void UpdateScoreCallback(int PlayerIndex, int Value);

        private BuzzDevice _buzz;
        private List<Sound> _sounds;
        private List<Player> _players;

        public FormControl()
        {
            InitializeComponent();
            InitEvents();
            InitSounds();
            ResizeWindow();

            _buzz = new BuzzDevice();
            _buzz.ButtonPressed += _buzz_ButtonPressed;
        }



        private void _buzz_ButtonPressed(object sender, BuzzButtonEventArgs e)
        {
            var speler = (_players == null ? null : (from p in _players where p.Index == e.Button.Player - 1 select p).SingleOrDefault());

            if (radioButtonOmHetSnelst.Checked && e.Button.Value == "R")
            {
                SendScores(); // ugh
                Display.OmHetSnelst(speler);
            }

            if (radioButtonKiesAntwoord.Checked && e.Button.Value != "R")
            {
                SendScores(); // ugh
                Display.KiesJuisteAntwoord(speler, e.Button);
            }

            // gamemaster
            if (radioButtonKiesAntwoord.Checked && e.Button.Player == 4 && checkBoxGamehostController.Checked)
            {
                if (e.Button.Value == "R")
                {
                    Display.Reset();
                }
                else
                {
                    var winnendeSpelers = Display.GetWinnendeSpelers(e.Button.Value);

                    if (winnendeSpelers != null)
                    {
                        foreach (var player in winnendeSpelers)
                        {
                            UpdateScore(player, 5);
                        }

                        Display.UpdateScore(e.Button.Value);
                    }
                }
            }

            // gamemaster
            if (radioButtonOmHetSnelst.Checked && e.Button.Player == 4 && checkBoxGamehostController.Checked && e.Button.Value != "R")
            {
                Display.Reset();
            }



        }

        private void UpdateScore(int PlayerIndex, int Value)
        {
            if (InvokeRequired)
            {
                Invoke(new UpdateScoreCallback(UpdateScore), new object[] { PlayerIndex, Value });
                return;
            }

            var scores = new List<NumericUpDown> { numericUpDownScore1, numericUpDownScore2, numericUpDownScore3, numericUpDownScore4 };

            scores[PlayerIndex].Value += Value;
            SendScores();
        }

        private void InitSounds()
        {
            int i = 0;

            _sounds = new List<Sound> {
                new Sound { Naam="Kip",  Locatie = "Chicken.wav" },
                new Sound { Naam="Koe",  Locatie = "Cow.wav" },
                new Sound { Naam="Olifant",  Locatie = "Elephant.wav" },
                new Sound { Naam="Geit",  Locatie = "Goat.wav" },
                new Sound { Naam="Leeuw",  Locatie = "Lion.wav" },
                new Sound { Naam="Aap",  Locatie = "Monkey.wav" },
                new Sound { Naam="Varken",  Locatie = "Pig.wav" },
            };

            foreach (ComboBox c in groupBoxTeams.Controls.OfType<ComboBox>())
            {
                c.Items.AddRange(_sounds.ToArray());
                c.SelectedIndex = 0;
                c.DisplayMember = "Naam";
                c.SelectedIndex = (i++);
                c.SelectedIndexChanged += TeamSound_SelectedIndexChanged;
            }
        }

        private void TeamSound_SelectedIndexChanged(object sender, EventArgs e)
        {
            PlaySound((Sound)(sender as ComboBox).SelectedItem);
        }

        public void PlaySound(Sound Sound)
        {
            if (checkBoxPlaySound.Checked)
            {
                new SoundPlayer("Sound/" + Sound.Locatie).Play();
            }
        }

        private void InitEvents()
        {
            foreach (RadioButton b in groupBoxMode.Controls)
            {
                b.CheckedChanged += Mode_CheckedChanged;
            }
        }

        private void Mode_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton b = sender as RadioButton;

            if (b.Checked == true)
            {
                SendScores();
                Display.SetMode(int.Parse(b.Tag.ToString()));
            }
        }

        private void SendScores()
        {
            if (InvokeRequired)
            {
                Invoke(new FunctionCallback(SendScores));
                return;
            }

            // Read the scores into a nice list, then send them to the display.
            // TODO: Bad programming if I ever saw it, but I need quick results; quiz is very soon :)
            _players = new List<Player>();
            _players.Add(new Player { Index = 0, Naam = textBoxGroep1.Text, Score = (int)numericUpDownScore1.Value, Geluid = (Sound)comboBox1.SelectedItem });
            _players.Add(new Player { Index = 1, Naam = textBoxGroep2.Text, Score = (int)numericUpDownScore2.Value, Geluid = (Sound)comboBox2.SelectedItem });
            _players.Add(new Player { Index = 2, Naam = textBoxGroep3.Text, Score = (int)numericUpDownScore3.Value, Geluid = (Sound)comboBox3.SelectedItem });

            if (!checkBoxGamehostController.Checked)
            {
                _players.Add(new Player { Index = 3, Naam = textBoxGroep4.Text, Score = (int)numericUpDownScore4.Value, Geluid = (Sound)comboBox4.SelectedItem });
            }

            Display.SetScore(_players);
        }

        private void ResizeWindow()
        {
            this.Size = new Size(Screen.PrimaryScreen.WorkingArea.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2);
            this.Location = new Point(0, Screen.PrimaryScreen.WorkingArea.Height / 2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Display.Vergroot();
        }

        private void FormControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control c in groupBoxTeams.Controls)
            {
                if (c.GetType() != typeof(NumericUpDown))
                {
                    c.Enabled = !checkBox1.Checked;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SendScores();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Display.Reset();
        }

        private void checkBoxGamehostController_CheckedChanged(object sender, EventArgs e)
        {
            var useControllerForGameHost = (sender as CheckBox).Checked;

            // disable laatste rij controllers indien nodig
            comboBox4.Visible = !useControllerForGameHost;
            textBoxGroep4.Visible = !useControllerForGameHost;
            numericUpDownScore4.Visible = !useControllerForGameHost;

            Display.nPlayers = (useControllerForGameHost ? 3 : 4);

            SendScores();
            Display.Reset();


        }
    }
}
