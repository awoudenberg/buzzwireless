﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuzzWireless
{
    public class BuzzColors
    {
        public static Color R { get { return Color.FromArgb(192, 0, 0); } }
        public static Color B { get { return Color.FromArgb(0, 192, 255); } }
        public static Color O { get { return Color.FromArgb(255, 128, 0); } }
        public static Color G { get { return Color.FromArgb(0, 192, 0); } }
        public static Color Y { get { return Color.Yellow; } }

        public static Color Get(string Name)
        {
            Color color;

            switch(Name)
            {
                case "R": color = BuzzColors.R; break;
                case "B": color = BuzzColors.B; break;
                case "O": color = BuzzColors.O; break;
                case "G": color = BuzzColors.G; break;
                case "Y": color = BuzzColors.Y; break;

                default: color = Color.Empty; break;
            }

            return color;
        }
    }
}
