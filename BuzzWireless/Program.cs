﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzzWireless
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var formControl = new FormControl();
            var formDisplay = new FormDisplay();
            var formTest = new FormHandsetTest();
            var threadControl = new Thread((ThreadStart)delegate { Application.Run(formControl); });
            var threadDisplay = new Thread((ThreadStart)delegate { Application.Run(formDisplay); });
            var threadTest = new Thread((ThreadStart)delegate { Application.Run(formTest); });

            formControl.Display = formDisplay;
            formControl.checkBoxGamehostController.Checked = true;
            formControl.radioButtonOmHetSnelst.Checked = true;

            threadControl.Start();
            threadDisplay.Start();
            threadTest.Start();
        }
    }
}
