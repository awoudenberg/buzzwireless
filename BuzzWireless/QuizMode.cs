﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuzzWireless
{
    public enum QuizMode
    {
        KiesEenAntwoord = 0,
        OmHetSnelst = 1,
        GoedOfFout = 2,
        Scores = 3
    }
}
