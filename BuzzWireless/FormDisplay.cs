﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzzWireless
{
    public partial class FormDisplay : Form
    {
        delegate void FunctionCallback();
        delegate void SetModeCallback(int Mode);
        delegate void SetScoreCallback(List<Player> Players);
        delegate void PlayerCallback(Player Player);
        delegate void UpdateCallback(string Value);
        delegate void KiesAntwoordCallback(Player Player, BuzzButton Knop);


        private QuizMode _mode;
        private List<int> _omHetSnelstSpelers;
        private List<string> _gekozenAntwoorden;

        public int nPlayers = 4;

        public FormDisplay()
        {
            InitializeComponent();
            ResizeWindow();
            Reset();
        }


        private void ResizeWindow()
        {
            this.Size = new Size(Screen.PrimaryScreen.WorkingArea.Width / 2, Screen.PrimaryScreen.WorkingArea.Height);
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2, 0);
        }

        public void SetMode(int Mode)
        {
            if (InvokeRequired)
            {
                Invoke(new SetModeCallback(SetMode), new object[] { Mode });
                return;
            }

            _mode = (QuizMode)Mode;
            panelScores.Visible = (_mode == QuizMode.Scores);
            panelOmHetSnelst.Visible = (_mode == QuizMode.OmHetSnelst || _mode == QuizMode.KiesEenAntwoord);

            switch (_mode)
            {
                case QuizMode.KiesEenAntwoord:
                    SetTitle("Welk antwoord is juist?");
                    BackColor = Color.Linen;
                    break;

                case QuizMode.OmHetSnelst:
                    SetTitle("Om het snelst");
                    BackColor = Color.MistyRose;
                    Reset();
                    break;

                case QuizMode.GoedOfFout:
                    SetTitle("Goed of fout?");
                    BackColor = Color.Azure;
                    break;

                case QuizMode.Scores:
                    SetTitle("Scores");
                    BackColor = Color.PaleGoldenrod;
                    break;
            }
        }

        public void UpdateScore(string Value)
        {
            if (InvokeRequired)
            {
                Invoke(new UpdateCallback(UpdateScore), new object[] { Value });
                return;
            }

            var controls = new List<Control> { label5, label6, label7, label8 }; // bugfix, order sometimes changes; still very hacky
            var kleuren = new Dictionary<string, string>();

            kleuren.Add("B", "A (blauw)");
            kleuren.Add("O", "B (oranje)");
            kleuren.Add("G", "C (groen)");
            kleuren.Add("Y", "D (geel)");


            for (int i = 0; i < nPlayers; i++)
            {
                controls[i].Visible = _gekozenAntwoorden[i] == Value;
            }

            labelJuist.Text = "Het juiste antwoord was " + kleuren[Value];
            labelJuist.BackColor = BuzzColors.Get(Value);
            labelJuist.Visible = true;
        }

        public void Reset()
        {
            if (InvokeRequired)
            {
                Invoke(new FunctionCallback(Reset));
                return;
            }

            _omHetSnelstSpelers = new List<int>();
            _gekozenAntwoorden = new List<string>(4) { "", "", "", "" };

            foreach (Control c in panelOmHetSnelst.Controls)
            {
                c.Visible = false;
                c.Text = "";
                c.BackColor = Color.Empty;
            }

            labelJuist.Visible = false;
        }

        public void OmHetSnelst(Player Speler)
        {
            if (Speler == null) { return; }

            if (InvokeRequired)
            {
                Invoke(new PlayerCallback(OmHetSnelst), new object[] { Speler });
                return;
            }

            if (!_omHetSnelstSpelers.Contains(Speler.Index))
            {
                var controls = new List<Control> { label5, label6, label7, label8 }; // bugfix, order sometimes changes; still very hacky
                var box = controls[_omHetSnelstSpelers.Count]; // ugh

                box.Text = Speler.Naam;
                box.Visible = true;

                _omHetSnelstSpelers.Add(Speler.Index);

                // speel het teamgeluid als ze de eerste waren
                if (_omHetSnelstSpelers.Count == 1)
                {
                    new SoundPlayer("Sound/" + Speler.Geluid.Locatie).Play();
                }
            }
        }


        public void KiesJuisteAntwoord(Player Speler, BuzzButton Knop)
        {
            if (Speler == null) { return; }

            if (InvokeRequired)
            {
                Invoke(new KiesAntwoordCallback(KiesJuisteAntwoord), new object[] { Speler, Knop });
                return;
            }

            if (_gekozenAntwoorden[Speler.Index] == "")
            {
                var controls = new List<Control> { label5, label6, label7, label8 }; // bugfix, order sometimes changes; still very hacky

                _gekozenAntwoorden[Speler.Index] = Knop.Value;
                controls[Speler.Index].Text = string.Format("{0} heeft gekozen", Speler.Naam);
                controls[Speler.Index].Visible = true;

                var AantalAntwoorden = (from a in _gekozenAntwoorden where a != "" select a).Count();
                var kleuren = new Dictionary<string, string>();

                kleuren.Add("B", "A (blauw)");
                kleuren.Add("O", "B (oranje)");
                kleuren.Add("G", "C (groen)");
                kleuren.Add("Y", "D (geel)");

                // Toon welke antwoorden ze hebben gekozen
                if (AantalAntwoorden == nPlayers)
                {
                    for (int i = 0; i < nPlayers; i++)
                    {
                        controls[i].Text += string.Format(" voor antwoord {0}", kleuren[_gekozenAntwoorden[i]]);
                        controls[i].BackColor = BuzzColors.Get(_gekozenAntwoorden[i]);
                    }
                }

            }
        }


        public List<int> GetWinnendeSpelers(string juisteAntwoord)
        {
            var lijst = new List<int>();
            var allesIngevuld = ((from s in _gekozenAntwoorden where s != "" select s).Count() == nPlayers);

            if (!allesIngevuld) { return null; }

            for (int i = 0; i < _gekozenAntwoorden.Count; i++)
            {
                if (_gekozenAntwoorden[i] == juisteAntwoord) { lijst.Add(i); }
            }

            return lijst;
        }

        private void SetTitle(string Title)
        {
            labelMode.Text = string.Format(" {0} ", Title);
        }

        public void Vergroot()
        {
            if (InvokeRequired)
            {
                Invoke(new FunctionCallback(Vergroot));
            }
            else
            {
                Size = Screen.AllScreens[1].WorkingArea.Size;
                Location = Screen.AllScreens[1].WorkingArea.Location;
                WindowState = FormWindowState.Maximized;
            }
        }

        private void FormDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        internal void SetScore(List<Player> Players)
        {
            if (InvokeRequired)
            {
                Invoke(new SetScoreCallback(SetScore), new object[] { Players });
                return;
            }

            var playersSorted = (from p in Players orderby p.Score descending select p).ToList();

            foreach (ProgressBar b in panelScores.Controls.OfType<ProgressBar>())
            {
                b.Maximum = playersSorted[0].Score;
            }

            label1.Text = string.Format("{0} {1}", playersSorted[0].Naam, playersSorted[0].Score);
            label2.Text = string.Format("{0} {1}", playersSorted[1].Naam, playersSorted[1].Score);
            label3.Text = string.Format("{0} {1}", playersSorted[2].Naam, playersSorted[2].Score);

            progressBar1.Value = playersSorted[0].Score;
            progressBar2.Value = playersSorted[1].Score;
            progressBar3.Value = playersSorted[2].Score;

            label4.Visible = (playersSorted.Count == 4);
            progressBar4.Visible = (playersSorted.Count == 4);


            if (playersSorted.Count == 4)
            {
                label4.Text = string.Format("{0} {1}", playersSorted[3].Naam, playersSorted[3].Score);
                progressBar4.Value = playersSorted[3].Score;
            }
        }
    }
}
