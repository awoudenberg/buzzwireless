﻿namespace BuzzWireless
{
    partial class FormControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBoxMode = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButtonOmHetSnelst = new System.Windows.Forms.RadioButton();
            this.radioButtonKiesAntwoord = new System.Windows.Forms.RadioButton();
            this.groupBoxTeams = new System.Windows.Forms.GroupBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.numericUpDownScore4 = new System.Windows.Forms.NumericUpDown();
            this.textBoxGroep4 = new System.Windows.Forms.TextBox();
            this.numericUpDownScore3 = new System.Windows.Forms.NumericUpDown();
            this.textBoxGroep3 = new System.Windows.Forms.TextBox();
            this.numericUpDownScore2 = new System.Windows.Forms.NumericUpDown();
            this.textBoxGroep2 = new System.Windows.Forms.TextBox();
            this.numericUpDownScore1 = new System.Windows.Forms.NumericUpDown();
            this.textBoxGroep1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxGamehostController = new System.Windows.Forms.CheckBox();
            this.checkBoxPlaySound = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBoxMode.SuspendLayout();
            this.groupBoxTeams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Gebruik tweede scherm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(179, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 118);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Algemeen";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(7, 79);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Reset";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 49);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Update score";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(9, 16);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "LockTeams";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupBoxMode
            // 
            this.groupBoxMode.Controls.Add(this.radioButton4);
            this.groupBoxMode.Controls.Add(this.radioButtonOmHetSnelst);
            this.groupBoxMode.Controls.Add(this.radioButtonKiesAntwoord);
            this.groupBoxMode.Location = new System.Drawing.Point(12, 13);
            this.groupBoxMode.Name = "groupBoxMode";
            this.groupBoxMode.Size = new System.Drawing.Size(161, 123);
            this.groupBoxMode.TabIndex = 2;
            this.groupBoxMode.TabStop = false;
            this.groupBoxMode.Text = "Modus";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(7, 66);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(58, 17);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "3";
            this.radioButton4.Text = "Scores";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButtonOmHetSnelst
            // 
            this.radioButtonOmHetSnelst.AutoSize = true;
            this.radioButtonOmHetSnelst.Location = new System.Drawing.Point(7, 43);
            this.radioButtonOmHetSnelst.Name = "radioButtonOmHetSnelst";
            this.radioButtonOmHetSnelst.Size = new System.Drawing.Size(89, 17);
            this.radioButtonOmHetSnelst.TabIndex = 1;
            this.radioButtonOmHetSnelst.TabStop = true;
            this.radioButtonOmHetSnelst.Tag = "1";
            this.radioButtonOmHetSnelst.Text = "Om het snelst";
            this.radioButtonOmHetSnelst.UseVisualStyleBackColor = true;
            // 
            // radioButtonKiesAntwoord
            // 
            this.radioButtonKiesAntwoord.AutoSize = true;
            this.radioButtonKiesAntwoord.Location = new System.Drawing.Point(7, 20);
            this.radioButtonKiesAntwoord.Name = "radioButtonKiesAntwoord";
            this.radioButtonKiesAntwoord.Size = new System.Drawing.Size(137, 17);
            this.radioButtonKiesAntwoord.TabIndex = 0;
            this.radioButtonKiesAntwoord.TabStop = true;
            this.radioButtonKiesAntwoord.Tag = "0";
            this.radioButtonKiesAntwoord.Text = "Kies het juiste antwoord";
            this.radioButtonKiesAntwoord.UseVisualStyleBackColor = true;
            // 
            // groupBoxTeams
            // 
            this.groupBoxTeams.Controls.Add(this.comboBox4);
            this.groupBoxTeams.Controls.Add(this.comboBox3);
            this.groupBoxTeams.Controls.Add(this.comboBox2);
            this.groupBoxTeams.Controls.Add(this.comboBox1);
            this.groupBoxTeams.Controls.Add(this.numericUpDownScore4);
            this.groupBoxTeams.Controls.Add(this.textBoxGroep4);
            this.groupBoxTeams.Controls.Add(this.numericUpDownScore3);
            this.groupBoxTeams.Controls.Add(this.textBoxGroep3);
            this.groupBoxTeams.Controls.Add(this.numericUpDownScore2);
            this.groupBoxTeams.Controls.Add(this.textBoxGroep2);
            this.groupBoxTeams.Controls.Add(this.numericUpDownScore1);
            this.groupBoxTeams.Controls.Add(this.textBoxGroep1);
            this.groupBoxTeams.Location = new System.Drawing.Point(12, 142);
            this.groupBoxTeams.Name = "groupBoxTeams";
            this.groupBoxTeams.Size = new System.Drawing.Size(515, 123);
            this.groupBoxTeams.TabIndex = 3;
            this.groupBoxTeams.TabStop = false;
            this.groupBoxTeams.Text = "Teams";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(7, 95);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(154, 21);
            this.comboBox4.TabIndex = 11;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(7, 69);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(154, 21);
            this.comboBox3.TabIndex = 10;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(7, 42);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(154, 21);
            this.comboBox2.TabIndex = 9;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(7, 16);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(154, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // numericUpDownScore4
            // 
            this.numericUpDownScore4.Location = new System.Drawing.Point(432, 95);
            this.numericUpDownScore4.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownScore4.Name = "numericUpDownScore4";
            this.numericUpDownScore4.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownScore4.TabIndex = 7;
            // 
            // textBoxGroep4
            // 
            this.textBoxGroep4.Location = new System.Drawing.Point(167, 94);
            this.textBoxGroep4.Name = "textBoxGroep4";
            this.textBoxGroep4.Size = new System.Drawing.Size(259, 20);
            this.textBoxGroep4.TabIndex = 6;
            this.textBoxGroep4.Text = "Speler D";
            // 
            // numericUpDownScore3
            // 
            this.numericUpDownScore3.Location = new System.Drawing.Point(432, 69);
            this.numericUpDownScore3.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownScore3.Name = "numericUpDownScore3";
            this.numericUpDownScore3.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownScore3.TabIndex = 5;
            // 
            // textBoxGroep3
            // 
            this.textBoxGroep3.Location = new System.Drawing.Point(167, 68);
            this.textBoxGroep3.Name = "textBoxGroep3";
            this.textBoxGroep3.Size = new System.Drawing.Size(259, 20);
            this.textBoxGroep3.TabIndex = 4;
            this.textBoxGroep3.Text = "Speler C";
            // 
            // numericUpDownScore2
            // 
            this.numericUpDownScore2.Location = new System.Drawing.Point(432, 43);
            this.numericUpDownScore2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownScore2.Name = "numericUpDownScore2";
            this.numericUpDownScore2.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownScore2.TabIndex = 3;
            // 
            // textBoxGroep2
            // 
            this.textBoxGroep2.Location = new System.Drawing.Point(167, 42);
            this.textBoxGroep2.Name = "textBoxGroep2";
            this.textBoxGroep2.Size = new System.Drawing.Size(259, 20);
            this.textBoxGroep2.TabIndex = 2;
            this.textBoxGroep2.Text = "Speler B";
            // 
            // numericUpDownScore1
            // 
            this.numericUpDownScore1.Location = new System.Drawing.Point(432, 17);
            this.numericUpDownScore1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownScore1.Name = "numericUpDownScore1";
            this.numericUpDownScore1.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownScore1.TabIndex = 1;
            // 
            // textBoxGroep1
            // 
            this.textBoxGroep1.Location = new System.Drawing.Point(167, 16);
            this.textBoxGroep1.Name = "textBoxGroep1";
            this.textBoxGroep1.Size = new System.Drawing.Size(259, 20);
            this.textBoxGroep1.TabIndex = 0;
            this.textBoxGroep1.Text = "Speler A";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxGamehostController);
            this.groupBox2.Controls.Add(this.checkBoxPlaySound);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Location = new System.Drawing.Point(347, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 118);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Instellingen";
            // 
            // checkBoxGamehostController
            // 
            this.checkBoxGamehostController.AutoSize = true;
            this.checkBoxGamehostController.Location = new System.Drawing.Point(9, 61);
            this.checkBoxGamehostController.Name = "checkBoxGamehostController";
            this.checkBoxGamehostController.Size = new System.Drawing.Size(155, 17);
            this.checkBoxGamehostController.TabIndex = 3;
            this.checkBoxGamehostController.Text = "Gebruik gamehostcontroller";
            this.checkBoxGamehostController.UseVisualStyleBackColor = true;
            this.checkBoxGamehostController.CheckedChanged += new System.EventHandler(this.checkBoxGamehostController_CheckedChanged);
            // 
            // checkBoxPlaySound
            // 
            this.checkBoxPlaySound.AutoSize = true;
            this.checkBoxPlaySound.Checked = true;
            this.checkBoxPlaySound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPlaySound.Location = new System.Drawing.Point(9, 38);
            this.checkBoxPlaySound.Name = "checkBoxPlaySound";
            this.checkBoxPlaySound.Size = new System.Drawing.Size(84, 17);
            this.checkBoxPlaySound.TabIndex = 2;
            this.checkBoxPlaySound.Text = "Speel geluid";
            this.checkBoxPlaySound.UseVisualStyleBackColor = true;
            // 
            // FormControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 434);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxTeams);
            this.Controls.Add(this.groupBoxMode);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormControl_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBoxMode.ResumeLayout(false);
            this.groupBoxMode.PerformLayout();
            this.groupBoxTeams.ResumeLayout(false);
            this.groupBoxTeams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScore1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxMode;
        private System.Windows.Forms.GroupBox groupBoxTeams;
        private System.Windows.Forms.NumericUpDown numericUpDownScore1;
        private System.Windows.Forms.TextBox textBoxGroep1;
        private System.Windows.Forms.NumericUpDown numericUpDownScore3;
        private System.Windows.Forms.TextBox textBoxGroep3;
        private System.Windows.Forms.NumericUpDown numericUpDownScore2;
        private System.Windows.Forms.TextBox textBoxGroep2;
        private System.Windows.Forms.NumericUpDown numericUpDownScore4;
        private System.Windows.Forms.TextBox textBoxGroep4;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxPlaySound;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.RadioButton radioButtonKiesAntwoord;
        public System.Windows.Forms.CheckBox checkBoxGamehostController;
        public System.Windows.Forms.RadioButton radioButtonOmHetSnelst;
    }
}