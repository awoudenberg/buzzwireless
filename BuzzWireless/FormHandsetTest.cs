﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzzWireless
{
    public partial class FormHandsetTest : Form
    {
        BuzzDevice Device = new BuzzDevice();
        
        public FormHandsetTest()
        {
            InitializeComponent();
            CreateHandsetViews();
            ResizeWindow();

            Device.ButtonChanged += Device_ButtonChanged;
        }

        private void ResizeWindow()
        {
            this.Size = new Size(Screen.PrimaryScreen.WorkingArea.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2);
            this.Location = new Point(0, 0);
        }

        private void Device_ButtonChanged(object sender, BuzzButtonEventArgs e)
        {
            foreach(UCHandset handset in panelHandsets.Controls)
            {
                handset.AlterButton(e);
            }
        }

        private void CreateHandsetViews()
        {
            var size = new Size(panelHandsets.Size.Width / 4, panelHandsets.Size.Height);
            var anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top;

            for (int i=0; i<4; i++)
            {
                var handset = new UCHandset(i + 1);

                handset.Anchor = anchor;
                handset.Size = size;
                handset.Location = new Point(size.Width * i, 0);
                handset.Visible = true;                

                panelHandsets.Controls.Add(handset);
            }
        }

       

        private void FormHandsetTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
