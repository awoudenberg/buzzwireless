﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuzzWireless
{
    public class Player
    {
        public string Naam { get; set; }
        public Sound Geluid { get; set; }
        public int Index { get; set; }
        public int Score { get; set; }

        public Player() { }

        public Player(string Naam, int Score)
        {
            this.Naam = Naam;
            this.Score = Score;
        }
    }
}
