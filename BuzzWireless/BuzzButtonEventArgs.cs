﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuzzWireless
{
    public class BuzzButtonEventArgs : EventArgs
    {
        public BuzzButton Button { get; set; }
        public bool State { get; set; }
        public DateTime Time { get; }

        public BuzzButtonEventArgs(BuzzButton Button, bool State)
        {
            this.Button = Button;
            this.State = State;

            Time = DateTime.Now;
        }
    }
}
