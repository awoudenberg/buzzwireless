﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzzWireless
{
    public partial class UCHandset : UserControl
    {
        delegate void AlterButtonsCallback(BuzzButtonEventArgs e);

        Dictionary<string, Button> Buttons = new Dictionary<string, Button>();
        int Player;

        public UCHandset()
        {
            InitializeComponent();
            Init();

            buttonR.BackColor = BuzzColors.R;
            buttonB.BackColor = BuzzColors.B;
            buttonO.BackColor = BuzzColors.O;
            buttonG.BackColor = BuzzColors.G;
            buttonY.BackColor = BuzzColors.Y;
        }

        public UCHandset(int Player)
        {
            InitializeComponent();
            Init(Player);
        }

        public void Init(int Player = 0)
        {
            this.Player = Player;
            this.groupBoxHandset.Text += Player.ToString();

            Buttons.Add("R", buttonR);
            Buttons.Add("B", buttonB);
            Buttons.Add("O", buttonO);
            Buttons.Add("G", buttonG);
            Buttons.Add("Y", buttonY);
        }


        

        public void AlterButton(BuzzButtonEventArgs e)
        {
            if (e.Button.Player == Player)
            {
                if (Buttons[e.Button.Value].InvokeRequired)
                {
                    Invoke(new AlterButtonsCallback(AlterButton), new object[] { e });
                }
                else
                {
                    Buttons[e.Button.Value].Text = (e.State ? "Pushed" : "");
                }
            }
        }
    }
}
