﻿namespace BuzzWireless
{
    partial class FormHandsetTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHandsets = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelHandsets
            // 
            this.panelHandsets.Location = new System.Drawing.Point(12, 12);
            this.panelHandsets.Name = "panelHandsets";
            this.panelHandsets.Size = new System.Drawing.Size(400, 300);
            this.panelHandsets.TabIndex = 4;
            // 
            // FormHandsetTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 322);
            this.Controls.Add(this.panelHandsets);
            this.Name = "FormHandsetTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Handset test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormHandsetTest_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelHandsets;
    }
}

