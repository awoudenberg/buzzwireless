﻿namespace BuzzWireless
{
    partial class UCHandset
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxHandset = new System.Windows.Forms.GroupBox();
            this.buttonB = new System.Windows.Forms.Button();
            this.buttonO = new System.Windows.Forms.Button();
            this.buttonG = new System.Windows.Forms.Button();
            this.buttonY = new System.Windows.Forms.Button();
            this.buttonR = new System.Windows.Forms.Button();
            this.groupBoxHandset.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxHandset
            // 
            this.groupBoxHandset.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxHandset.Controls.Add(this.buttonB);
            this.groupBoxHandset.Controls.Add(this.buttonO);
            this.groupBoxHandset.Controls.Add(this.buttonG);
            this.groupBoxHandset.Controls.Add(this.buttonY);
            this.groupBoxHandset.Controls.Add(this.buttonR);
            this.groupBoxHandset.Location = new System.Drawing.Point(3, 3);
            this.groupBoxHandset.Name = "groupBoxHandset";
            this.groupBoxHandset.Size = new System.Drawing.Size(141, 280);
            this.groupBoxHandset.TabIndex = 5;
            this.groupBoxHandset.TabStop = false;
            this.groupBoxHandset.Text = "Handset ";
            // 
            // buttonB
            // 
            this.buttonB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonB.Location = new System.Drawing.Point(7, 100);
            this.buttonB.Name = "buttonB";
            this.buttonB.Size = new System.Drawing.Size(128, 39);
            this.buttonB.TabIndex = 5;
            this.buttonB.UseVisualStyleBackColor = false;
            // 
            // buttonO
            // 
            this.buttonO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonO.Location = new System.Drawing.Point(7, 145);
            this.buttonO.Name = "buttonO";
            this.buttonO.Size = new System.Drawing.Size(128, 39);
            this.buttonO.TabIndex = 4;
            this.buttonO.UseVisualStyleBackColor = false;
            // 
            // buttonG
            // 
            this.buttonG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonG.Location = new System.Drawing.Point(7, 190);
            this.buttonG.Name = "buttonG";
            this.buttonG.Size = new System.Drawing.Size(128, 39);
            this.buttonG.TabIndex = 3;
            this.buttonG.UseVisualStyleBackColor = false;
            // 
            // buttonY
            // 
            this.buttonY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonY.BackColor = System.Drawing.Color.Yellow;
            this.buttonY.Location = new System.Drawing.Point(7, 235);
            this.buttonY.Name = "buttonY";
            this.buttonY.Size = new System.Drawing.Size(128, 39);
            this.buttonY.TabIndex = 2;
            this.buttonY.UseVisualStyleBackColor = false;
            // 
            // buttonR
            // 
            this.buttonR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonR.Location = new System.Drawing.Point(7, 20);
            this.buttonR.Name = "buttonR";
            this.buttonR.Size = new System.Drawing.Size(128, 74);
            this.buttonR.TabIndex = 0;
            this.buttonR.UseVisualStyleBackColor = false;
            // 
            // UCHandset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxHandset);
            this.Name = "UCHandset";
            this.Size = new System.Drawing.Size(147, 286);
            this.groupBoxHandset.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxHandset;
        private System.Windows.Forms.Button buttonB;
        private System.Windows.Forms.Button buttonO;
        private System.Windows.Forms.Button buttonG;
        private System.Windows.Forms.Button buttonY;
        private System.Windows.Forms.Button buttonR;
    }
}
